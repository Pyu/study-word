import {Component } from 'react'
import { View } from '@tarojs/components'
import Taro, { getCurrentInstance } from '@tarojs/taro'
import TaroWemark from '@/conponents/taro-wemark/taro-wemark'
import './contentPage.scss'

export default class ES6Content extends Component {
  constructor (props) {
    super(props)
    this.state ={
      type: getCurrentInstance().router.params.type
    }
  }
  componentWillMount () {
    Taro.setNavigationBarTitle({
      title: `ES6手册-${getCurrentInstance().router.params.name}`
    })
  }
  onShareAppMessage () {
    return {
      title: `ES6手册-${getCurrentInstance().router.params.name}`,
      path: `/pages/ES6/contentPage/contentPage?type=${this.state.type}&name=${getCurrentInstance().router.params.name}`
    }
  }
  onShareTimeline () {
    return {
      title: `ES6手册-${getCurrentInstance().router.params.name}`,
      path: `/pages/ES6/contentPage/contentPage?type=${this.state.type}&name=${getCurrentInstance().router.params.name}`
    }
  }
  render () {
    const {type} = this.state
    return (
      <View className='content-box'>
        <TaroWemark md={require(`../../../DOC/ES6/${type}.md`)} />
      </View>
    )
  }
}