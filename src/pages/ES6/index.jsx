import { View } from '@tarojs/components'
import {Component} from 'react'
import Taro,{ } from '@tarojs/taro'
import './index.scss'

export default class es6Index extends Component {
  constructor (props) {
    super(props)
    this.state = {
      menuList: [
        { title: 'ECMAScript 6 简介', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'intro'},
        { title: 'let 和 const 命令', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'let'},
        { title: '变量的解构赋值', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'destructuring'},
        { title: '字符串的扩展', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'string'},
        { title: '字符串的新增方法', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'string-methods'},
        { title: '正则的扩展', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'regex'},
        { title: '数值的扩展', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'number'},
        { title: '函数的扩展', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'function'},
        { title: '数组的扩展', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'array'},
        { title: '对象的扩展', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'object'},
        { title: '对象的新增方法', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'object-methods'},
        { title: 'Symbol', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'symbol'},
        { title: 'Set 和 Map 数据结构', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'set-map'},
        { title: 'Proxy', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'proxy'},
        { title: 'Reflect', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'reflect'},
        { title: 'Promise 对象', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'promise'},
        { title: 'Iterator 和 for...of 循环', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'iterator'},
        { title: 'Generator 函数的语法', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'generator'},
        { title: 'Generator 函数的异步应用', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'generator-async'},
        { title: 'async 函数', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'async'},
        { title: 'Class 的基本语法', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'class'},
        { title: 'Class 的继承', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'class-extends'},
        { title: 'Module 的语法', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'module'},
        { title: 'Module 的加载实现', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'module-loader'},
        { title: '编程风格', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'style'},
        { title: '读懂 ECMAScript 规格', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'spec'},
        { title: '异步遍历器', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'async-iterator'},
        { title: 'ArrayBuffer', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'arraybuffer'},
        { title: '最新提案', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'proposals'},
        { title: 'Decorator', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'decorator'},
        { title: '参考链接', desc: '', link: '/pages/ES6/contentPage/contentPage', type: 'reference'},
      ]
    }
  }
  onShareAppMessage () {
    return {
      title: 'ECMAScript 6 入门教程',
      path: '/pages/ES6/index'
    }
  }
  onShareTimeline () {
    return {
      title: 'ECMAScript 6 入门教程',
      path: '/pages/ES6/index'
    }
  }
  jumpToDetail=(item)=>{
    console.log(item)
    Taro.navigateTo({
      url: `${item.link}?type=${item.type}&name=${item.title}`,
    })
  }
  render () {
    const {menuList} = this.state
    return (
      <View className='es6-page'>
        <View className='es6-page--title'>ECMAScript 6 入门教程</View>
        <View className='es6-page--author'>作者：阮一峰</View>
        <View className='es6-page--copyright'>授权：署名-非商用许可证</View>

        <View className='es6-page-menuList'>
          {menuList.map(item=>{
            return <View key={item.type} className='es6-page-menuList--item' onClick={this.jumpToDetail.bind(this,item)}>{item.title}</View>
          })}
        </View>
      </View>
    )
  }
}

