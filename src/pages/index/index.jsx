import { Component } from 'react'
import { View } from '@tarojs/components'
import Taro from '@tarojs/taro'
import './index.scss'

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classList: [
        {
          title: 'ECMAScript 6 入门教程',
          desc: '',
          link: '/pages/ES6/index'
        }
      ]
    };
  }
  
  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
  onShareAppMessage (res) {
    return {
      title: '前端学习手册',
      path: '/pages/index/index'
    }
  }
  onShareTimeline () {
    return {
      title: '前端学习手册',
      path: '/pages/index/index'
    }
  }
  jumpToapage(link) {
    if(!link){
      return Taro.showToast({
        title: '暂时未开放...',
        icon: 'none',
        duration: 2000
      })
    }
    Taro.navigateTo({
      url: link,
    })
  }
  render () {
    const {classList} = this.state
    return (
      <View className='index-content'>
        <View className='index-content--title'>前端学习手册</View>
        <View className='index-content-list'>
          {
            classList.map(item=>{
              return <View key={item.title} className='index-content-list--item' onClick={this.jumpToapage.bind(this,item.link)}>{item.title}</View>
            })
          }
        </View>
        
      </View>
    )
  }
}
