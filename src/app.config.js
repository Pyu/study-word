export default {
  pages: [
    "pages/index/index",
    "pages/ES6/index",
    "pages/ES6/contentPage/contentPage"
  ],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "",
    navigationBarTextStyle: "black"
  }
};
